require("@nomiclabs/hardhat-waffle");
const polygonProjectId = 'Put polygon project id';
const projectId = 'Put infura project id';
const fs = require('fs')
const keyData = fs.readFileSync('./p-key.txt', {
  encoding:'utf8', flag:'r'
})

module.exports = {
  defaultNetwork: 'hardhat',
  networks:{
    hardhat:{
      chainId: 1337 // config standard 
    },
    rinkeby: { //// infuraでpolygon test netがaddonだったのでこっち使ってテスト
      url: `https://rinkeby.infura.io/v3/${projectId}`,
      accounts: [keyData]
    },
    mumbai:{ // infuraでmumbai test netを入れたもの
      url:`https://polygon-mumbai.infura.io/v3/${projectId}`,
      accounts:[keyData] // metamaskのprivate key
    },
    mainnet: {
      url:`https://mainnet.infura.io/v3/${projectId}`,
      accounts:[keyData]
    }
  },
  solidity: {
    version: "0.8.4",
    settings: {
      optimizer: { // 無駄な箇所をきれいにしてgasコスト抑える
        enabled: true, 
        runs: 200
      }
    }
  }
};
