import '../styles/globals.css'
import './app.css'
import Link from 'next/link'

function Marketplace({Component, pageProps}) {
  return (
    <div>
      <nav className='border-b p-6' style={{backgroundColor:'black'}}>
        <p className='text-4x1 font-bold text-white'>Matsu NFT Marketplace</p>
        <div className='flex mt-4 justify-center'>
          <Link href='/'>
            <a className='mr-12' style={{fontSize: '16px'}}>
              Pirmary Marketplace
            </a>
          </Link>
          <Link href='/nft-generation'>
            <a className='mr-12'>
              Mint Tokens
            </a>
          </Link>
          <Link href='/my-nfts'>
            <a className='mr-12'>
              My NFTs
            </a>
          </Link>
          <Link href='/every-nfts'>
            <a className='mr-6'>
              Every NFTs
            </a>
          </Link>
          </div>
      </nav>
      <Component {...pageProps} style={{backgroundColor:'grey'}}/>
    </div>
  )
}

export default Marketplace
