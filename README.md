## Sample Hardhat NFT Mint Project

This project demonstrates a basic Hardhat use case. 
It comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts.

## Manage private key
Rename `p-key-sample.txt` to `p-key.txt` and set private key in it.

### Infura
https://infura.io/dashboard

## Hardhat command

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```


## Develop on local environment

1. Run local node
```shell
npx hardhat node
```
2. Create Account by importing private keys geven by No.1 operation. (Metamask Import)

3. Compile
```
npx hardhat compile
```
4. Set private key of the account to `p-key.txt` (Create account in Metamask if you don't have one.)

5. Deploy Smart Contract on local node given by Hardhat
```
npx hardhat run scripts/deploy.js --network localhost
```
After deploy scceeed, smart contracts address will be written in `config.js`
(If you'd like to connect to testnet after developing local env, you have to update config.js)

4. Launch Client Side
```
npm run dev
```
